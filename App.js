import { createStackNavigator } from 'react-navigation';
import Home from './app/screens/home';
import Example1 from './app/screens/example1';
import Example2 from './app/screens/example2';
import Example3 from './app/screens/example3';

export default createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      headerMode: 'none',
      header: null
    }
  },
  Example1: {
    screen: Example1,
    navigationOptions: {
      title: 'Load SVG (local & server)'
    }
  },
  Example2: {
    screen: Example2,
    navigationOptions: {
      title: 'Customization'
    }
  },
  Example3: {
    screen: Example3,
    navigationOptions: {
      title: 'Resolution'
    }
  },
});