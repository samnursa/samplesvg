/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {ScrollView, TouchableOpacity, Text} from 'react-native';
// import SvgUri from 'react-native-svg-uri';
// import SvgCta from '../../components/svg/Cta';
import styles from './style';

export default class App extends Component {
  render() {
    return (
      <ScrollView>
          <TouchableOpacity style={styles.container1} onPress={()=>this.props.navigation.navigate('Example1')}>
            <Text style={styles.text}>Load SVG (local & server)</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.container2} onPress={()=>this.props.navigation.navigate('Example2')}>
            <Text style={styles.text}>Customization</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.container3} onPress={()=>this.props.navigation.navigate('Example3')}>
            <Text style={styles.text}>Resolution</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.container4}>
            <Text style={styles.text}>Cache Image</Text>
          </TouchableOpacity>
      </ScrollView>
    );
  }
}
