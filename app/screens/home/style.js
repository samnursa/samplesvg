import { StyleSheet } from 'react-native';

const tempContainer = {
    flex:1, 
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5,
    borderRadius: 10
}

export default StyleSheet.create({
  container1: {
    ...tempContainer,
    backgroundColor: '#4db8ff'
  },
  container2: {
    ...tempContainer,
    backgroundColor: '#ffff4d'
  },
  container3: {
    ...tempContainer,
    backgroundColor: '#ff751a'
  },
  container4: {
    ...tempContainer,
    backgroundColor: '#33ff33'
  },
  text: {
    fontSize:30
  }
});
