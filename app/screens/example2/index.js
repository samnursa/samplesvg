import React, {Component} from 'react';
import {ScrollView, View, Text} from 'react-native';
import SvgUri from 'react-native-svg-uri';
import SvgCta from '../../components/svg/Cta';

export default class App extends Component {
  render() {
    return (
      <ScrollView style={{flex: 1}}>
        <View style={{ justifyContent: 'center',alignItems:'center', marginTop: 50}}>
          <SvgCta color='#ff0066'/>
          <SvgCta color='#4db8ff'/>
          <SvgCta color='#ffff4d'/>
        </View>
      </ScrollView>
    );
  }
}
