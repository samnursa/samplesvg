import React, {Component} from 'react';
import {ScrollView, View, Text} from 'react-native';
import SvgUri from 'react-native-svg-uri';
import SvgCta from '../../components/svg/Cta';

export default class App extends Component {
  render() {
    return (
      <ScrollView style={{flex: 1}}>
        <View style={{ justifyContent: 'center',alignItems:'center', marginTop: 50}}>
          <SvgUri
            width="200"
            height="200"
            source={{uri:'http://thenewcode.com/assets/images/thumbnails/homer-simpson.svg'}}
          />
          <SvgCta color='#ff0066'/>
        </View>
      </ScrollView>
    );
  }
}
