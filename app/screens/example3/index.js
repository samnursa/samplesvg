import React, {Component} from 'react';
import {ScrollView, View, Image} from 'react-native';
import SvgCta from '../../components/svg/Cta';

export default class App extends Component {
  render() {
    return (
      <ScrollView style={{flex: 1}}>
        <View style={{ justifyContent: 'center',alignItems:'center', marginTop: 50}}>
          <Image style={{width:200, height:200}} source={require('../../../assets/images/CTA.png')}/>
          <SvgCta color='#ff0066'/>
        </View>
      </ScrollView>
    );
  }
}
