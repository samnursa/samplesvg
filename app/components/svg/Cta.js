import React from "react";
import Svg, { G, Circle, Defs } from "react-native-svg";

// SVGR has dropped some elements not supported by react-native-svg: filter, feFlood, feColorMatrix, feOffset, feGaussianBlur, feBlend
const SvgCta = props => (
  <Svg width={150} height={150} fill="none">
    <G filter="url(#filter0_d)">
      <Circle cx={75} cy={65} r={55} fill={props.color} />
    </G>
    <Defs />
  </Svg>
);

export default SvgCta;
