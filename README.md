How to install SVGR : 

1. Open terminal, go to directory project install "npx @svgr/cli"
2. add new file .svgo.yml

3. write code bellow in .svgo.yml

multipass: true

plugins:
  /- removeViewBox: false

(without / before -)

4. open package.json and add 

  "scripts": {
    ...
    "convertSvgToJs": "npx @svgr/cli --native -d app/components/svg assets/images"
  },
  "dependencies": {
    ...
  }
  
5. make folder app/components/svg to save result from convert SVG to JS
6. make folder assets/images and store all your SVG image
7. write "npm run convertSvgToJs" on your terminal